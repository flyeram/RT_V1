/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 12:46:20 by tbalu             #+#    #+#             */
/*   Updated: 2016/02/25 15:25:32 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <libft.h>
#include <unistd.h>
#include <rt_v.h>
#include <stdlib.h>

void		create_functions(t_env *env)
{
	env->functions[0] = &intersect_sphere;
	env->functions[1] = &intersect_plane;
	env->functions[2] = &intersect_cylinder;
	env->functions[3] = &intersect_cone;
	env->n_fct[0] = &find_normal_sphere;
	env->n_fct[1] = &find_normal_plane;
	env->n_fct[2] = &find_normal_cylinder;
	env->n_fct[3] = &find_normal_cone;
}

/*
**function which initialize the events and start the loop
*/

void		executor(t_env *env)
{
	draw_loop(env);
	mlx_expose_hook((*env).win, expose, env);
	mlx_hook((*env).win, 2, (1L << 0), press_key, env);
	mlx_loop((*env).mlx);
}

int			main(int ac, char **av)
{
	t_env	*env;
	int		win_x;
	int		win_y;

	if (ac != 2)
		return (0);
	win_x = 640;
	win_y = 420;
	env = constructor_env(win_x, win_y);
	env->camera->origin = create_vector(500, -100.0f, -1000.0f);
	env->scene = constructor_scene(av[1]);
	create_functions(env);
	env->win_ratio = (double)env->win_size.x / (double)env->win_size.y;
	if (env->scene == NULL)
		return (0);
	executor(env);
	return (0);
}

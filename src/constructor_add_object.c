/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   constructor_add_object.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/03 14:45:10 by tbalu             #+#    #+#             */
/*   Updated: 2016/02/26 10:44:17 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <rt_v.h>
#include <stdlib.h>

int			constructor_add_sphere(char **line_split, t_scene *scene,
			t_tmp *obj_val)
{
	int		nb;

	if (ft_arraylen(line_split) != 5)
		return (-1);
	scene->obj_count++;
	nb = scene->obj_count;
	if (nb >= obj_val->obj_tmp_count)
		return (-1);
	if (!(scene->obj_tab[nb] = (t_obj *)malloc(sizeof(t_obj))))
		return (-1);
	scene->obj_tab[nb]->pos = create_vector(
	ft_stof(line_split[0]), ft_stof(line_split[1]), ft_stof(line_split[2]));
	scene->obj_tab[nb]->radius = ft_stof(line_split[3]);
	scene->obj_tab[nb]->material = ft_atoi(line_split[4]);
	if (scene->obj_tab[nb]->material >= obj_val->material_tmp_count
	|| scene->obj_tab[nb]->material < 0)
		return (-1);
	scene->obj_tab[nb]->type = 0;
	return (1);
}

int			constructor_add_plane(char **line_split, t_scene *scene,
			t_tmp *obj_val)
{
	int		nb;

	if (ft_arraylen(line_split) != 7)
		return (-1);
	scene->obj_count++;
	nb = scene->obj_count;
	if (nb >= obj_val->obj_tmp_count)
		return (-1);
	if (!(scene->obj_tab[nb] = (t_obj *)malloc(sizeof(t_obj))))
		return (-1);
	scene->obj_tab[nb]->pos = create_vector(
	ft_stof(line_split[0]), ft_stof(line_split[1]), ft_stof(line_split[2]));
	scene->obj_tab[nb]->normale = create_vector(
	ft_stof(line_split[3]), ft_stof(line_split[4]), ft_stof(line_split[5]));
	scene->obj_tab[nb]->normale = normalize(&scene->obj_tab[nb]->normale);
	scene->obj_tab[nb]->material = ft_atoi(line_split[6]);
	if (scene->obj_tab[nb]->material >= obj_val->material_tmp_count
	|| scene->obj_tab[nb]->material < 0)
		return (-1);
	scene->obj_tab[nb]->type = 1;
	return (1);
}

int			constructor_add_cylinder(char **line_split, t_scene *scene,
			t_tmp *obj_val, int type)
{
	int		nb;

	if (ft_arraylen(line_split) != 8)
		return (-1);
	scene->obj_count++;
	nb = scene->obj_count;
	if (nb >= obj_val->obj_tmp_count)
		return (-1);
	if (!(scene->obj_tab[nb] = (t_obj *)malloc(sizeof(t_obj))))
		return (-1);
	scene->obj_tab[nb]->pos = create_vector(
	ft_stof(line_split[0]), ft_stof(line_split[1]), ft_stof(line_split[2]));
	scene->obj_tab[nb]->normale = create_vector(
	ft_stof(line_split[3]), ft_stof(line_split[4]), ft_stof(line_split[5]));
	scene->obj_tab[nb]->normale = normalize(&scene->obj_tab[nb]->normale);
	scene->obj_tab[nb]->radius = ft_stof(line_split[6]);
	scene->obj_tab[nb]->material = ft_atoi(line_split[7]);
	if (scene->obj_tab[nb]->material >= obj_val->material_tmp_count
	|| scene->obj_tab[nb]->material < 0)
		return (-1);
	scene->obj_tab[nb]->type = type;
	return (1);
}

int			constructor_add_light(char **line_split, t_scene *scene,
			t_tmp *obj_val)
{
	int		nb;

	if (ft_arraylen(line_split) != 6)
		return (-1);
	scene->light_count++;
	nb = scene->light_count;
	if (nb >= obj_val->light_tmp_count)
		return (-1);
	if (!(scene->light_tab[nb] = (t_light *)malloc(sizeof(t_light))))
		return (-1);
	scene->light_tab[nb]->pos = create_vector(
	ft_stof(line_split[0]), ft_stof(line_split[1]), ft_stof(line_split[2]));
	scene->light_tab[nb]->intensity = color_percent(
	ft_stof(line_split[3]), ft_stof(line_split[4]), ft_stof(line_split[5]));
	return (1);
}

int			constructor_add_material(char **line_split, t_scene *scene,
			t_tmp *obj_val)
{
	int		nb;

	if (ft_arraylen(line_split) != 4)
		return (-1);
	scene->material_count++;
	nb = scene->material_count;
	if (nb >= obj_val->material_tmp_count)
		return (-1);
	if (!(scene->material_tab[nb] = (t_material *)malloc(sizeof(t_material))))
		return (-1);
	scene->material_tab[nb]->diffuse = color_percent(
	ft_stof(line_split[0]), ft_stof(line_split[1]), ft_stof(line_split[2]));
	scene->material_tab[nb]->ref = ft_stof(line_split[3]);
	return (1);
}

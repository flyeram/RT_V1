/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cone_formula.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/15 16:22:32 by tbalu             #+#    #+#             */
/*   Updated: 2016/02/25 13:10:49 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <rt_v.h>

int			find_normal_cone(t_env *env, int current)
{
	t_obj		*cone;
	t_vector	scale;
	t_vector	diff;
	t_vector	tmp;
	double		m;

	scale = vector_scale(&env->data_scene->ray->dir, env->data_scene->t);
	env->data_scene->new_start = add_vector(&scale,
	&env->data_scene->ray->start);
	cone = env->scene->obj_tab[current];
	diff = substract_vector(&env->data_scene->ray->start, &cone->pos);
	m = dot_product(&env->data_scene->ray->dir, &cone->normale)
	* env->data_scene->t + dot_product(&diff, &cone->normale);
	env->data_scene->normal = vector_scale(&env->data_scene->ray->dir,
	env->data_scene->t);
	env->data_scene->normal = add_vector(&env->data_scene->normal, &diff);
	tmp = vector_scale(&cone->normale, m * (1 + pow(tan(cone->radius / 2), 2)));
	env->data_scene->normal = substract_vector(&env->data_scene->normal, &tmp);
	env->data_scene->normal = normalize(&env->data_scene->normal);
	return (0);
}

int			intersect_cone(t_obj *cone, t_ray *ray, double *t)
{
	double		abc[3];
	double		discr;
	t_vector	diff;
	double		tmp[2];
	double		tangle;

	tangle = tan(cone->radius / 2.00f);
	diff = substract_vector(&ray->start, &cone->pos);
	tmp[0] = dot_product(&ray->dir, &cone->normale);
	tmp[1] = dot_product(&diff, &cone->normale);
	abc[0] = dot_product(&ray->dir, &ray->dir) -
	((1 + pow(tangle, 2)) * pow(tmp[0], 2));
	abc[1] = 2 * (dot_product(&ray->dir, &diff) -
	((1 + pow(tangle, 2)) * tmp[0] * tmp[1]));
	abc[2] = dot_product(&diff, &diff) -
	((1 + pow(tangle, 2)) * pow(tmp[1], 2));
	discr = abc[1] * abc[1] - (4 * abc[0] * abc[2]);
	if (discr < 0.00001f)
		return (-1);
	else
		return (resolv_quadratic(discr, t, abc[1], abc[0]));
}

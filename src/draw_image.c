/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_image.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/02 18:51:46 by tbalu             #+#    #+#             */
/*   Updated: 2016/02/26 11:09:50 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rt_v.h>
#include <stdlib.h>
#include <libft.h>
#include <math.h>

/*
** function to initialize all the variable for each iteration
*/

void		initialize_draw(t_env *env, int x, int y)
{
	double		xx;
	double		yy;

	xx = (2 * ((double)x / (double)env->win_size.x) - 1) *
	env->win_ratio * ZOOM;
	yy = (1 - 2 * ((double)y / (double)env->win_size.y)) * ZOOM;
	env->data_scene->ray->dir.x = xx * cos(env->yaw) * cos(env->pitch) +
	yy * (cos(env->yaw) * sin(env->pitch) * sin(env->roll) - sin(env->yaw) *
	cos(env->roll)) + cos(env->yaw) * sin(env->pitch) * cos(env->roll) +
	sin(env->yaw) * sin(env->roll);
	env->data_scene->ray->dir.y = xx * sin(env->yaw) * cos(env->pitch) + yy *
	(sin(env->yaw) * sin(env->pitch) * sin(env->roll) + cos(env->yaw) *
	cos(env->roll)) + sin(env->yaw) * sin(env->pitch) * cos(env->roll) -
	cos(env->yaw) * sin(env->roll);
	env->data_scene->ray->dir.z = xx * -sin(env->pitch) + yy * cos(env->pitch)
	* sin(env->roll) + cos(env->pitch) * cos(env->roll);
	env->data_scene->ray->dir = normalize(&env->data_scene->ray->dir);
	env->data_scene->ray->start = env->camera->origin;
	env->data_scene->level = 0;
	env->data_scene->coef = 1.0;
	env->data_scene->color_final.r = 0.0;
	env->data_scene->color_final.g = 0.0;
	env->data_scene->color_final.b = 0.0;
}

int			initialize_data_scene(t_env *env)
{
	t_data		*data;
	t_ray		*ray;

	if (!(ray = (t_ray *)malloc(sizeof(t_ray))))
		return (-1);
	ray->start = create_vector(0.0, 0.0, 0.0);
	ray->dir = create_vector(0.0, 0.0, 0.0);
	if (!(data = (t_data *)malloc(sizeof(t_data))))
		return (-1);
	data->ray = ray;
	data->color_final = color_percent(0, 0, 0);
	env->data_scene = data;
	return (1);
}

/*
**function where we check if the ray hit an object or not
*/

int			check_object(t_env *env, t_ray *ray)
{
	int		i;
	int		current_object;

	i = 0;
	current_object = -1;
	while (i < env->scene->obj_count)
	{
		if (env->functions[env->scene->obj_tab[i]->type]
(env->scene->obj_tab[i], ray, &(env->data_scene->t)) == 0)
			current_object = i;
		i++;
	}
	return (current_object);
}

/*
**function which launch all the other function like shadow, light and object
*/

void		draw_each(t_env *env)
{
	int			cur;
	double		reflect;
	t_vector	tmp;

	while ((env->data_scene->coef > 0.0f && env->data_scene->level < 2))
	{
		env->data_scene->t = 20000.0f;
		if ((cur = check_object(env, env->data_scene->ray)) == -1)
			return ;
		if ((env->n_fct[env->scene->obj_tab[cur]->type](env, cur)) == -1)
			return ;
		light_function(env, cur);
		env->data_scene->coef *=
		env->scene->material_tab[env->scene->obj_tab[cur]->material]->ref;
		env->data_scene->ray->start = env->data_scene->new_start;
		reflect = 2.0f * dot_product(&env->data_scene->ray->dir,
		&env->data_scene->normal);
		tmp = vector_scale(&env->data_scene->normal, reflect);
		env->data_scene->ray->dir =
		substract_vector(&env->data_scene->ray->dir, &tmp);
		env->data_scene->level++;
	}
}

/*
**main function to draw, initialize variables and launch the loop for each pixel
*/

void		draw_loop(t_env *env)
{
	int		x;
	int		y;

	if ((initialize_data_scene(env)) == -1)
		return ;
	y = 0;
	while (y < env->win_size.y)
	{
		x = 0;
		while (x < env->win_size.x)
		{
			initialize_draw(env, x, y);
			draw_each(env);
			image_put_pixel(*env, x, y, create_color(0,
			ft_min(env->data_scene->color_final.r * 255.0f, 255.0f),
			ft_min(env->data_scene->color_final.g * 255.0f, 255.0f),
			ft_min(env->data_scene->color_final.b * 255.0f, 255.0f)));
			x++;
		}
		y++;
	}
}

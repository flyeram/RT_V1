/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cylinder_formula.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 16:33:17 by tbalu             #+#    #+#             */
/*   Updated: 2016/02/25 13:09:50 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <rt_v.h>

int			find_normal_cylinder(t_env *env, int current)
{
	double		m;
	t_vector	diff;
	t_obj		*cylin;
	t_vector	tmp;
	t_vector	scale;

	scale = vector_scale(&env->data_scene->ray->dir, env->data_scene->t);
	env->data_scene->new_start = add_vector(&scale,
	&env->data_scene->ray->start);
	cylin = env->scene->obj_tab[current];
	diff = substract_vector(&env->data_scene->ray->start, &cylin->pos);
	m = dot_product(&env->data_scene->ray->dir, &cylin->normale)
	* env->data_scene->t + dot_product(&diff, &cylin->normale);
	env->data_scene->normal = vector_scale(&env->data_scene->ray->dir,
	env->data_scene->t);
	env->data_scene->normal = add_vector(&env->data_scene->normal, &diff);
	tmp = vector_scale(&cylin->normale, m);
	env->data_scene->normal = substract_vector(&env->data_scene->normal, &tmp);
	env->data_scene->normal = normalize(&env->data_scene->normal);
	return (0);
}

int			intersect_cylinder(t_obj *cylin, t_ray *ray, double *t)
{
	double		abc[3];
	double		discr;
	t_vector	diff;
	double		tmp[2];

	diff = substract_vector(&ray->start, &cylin->pos);
	tmp[0] = dot_product(&ray->dir, &cylin->normale);
	tmp[1] = dot_product(&diff, &cylin->normale);
	abc[0] = dot_product(&ray->dir, &ray->dir) - pow(tmp[0], 2);
	abc[1] = 2 * (dot_product(&ray->dir, &diff) - (tmp[0] * tmp[1]));
	abc[2] = dot_product(&diff, &diff) - pow(tmp[1], 2) - pow(cylin->radius, 2);
	discr = abc[1] * abc[1] - (4 * abc[0] * abc[2]);
	if (discr < 0)
		return (-1);
	else
		return (resolv_quadratic(discr, t, abc[1], abc[0]));
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_v.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbalu <tbalu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/16 12:57:32 by tbalu             #+#    #+#             */
/*   Updated: 2016/02/25 14:40:54 by tbalu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RT_V_H
# define RT_V_H
# include <typedef_rt_v.h>

struct			s_vector
{
	double		x;
	double		y;
	double		z;
};

struct			s_ray
{
	t_vector	start;
	t_vector	dir;
};

struct			s_obj
{
	t_vector	pos;
	t_vector	normale;
	double		radius;
	int			material;
	int			type;
};

struct			s_image
{
	char		*cimg;
	void		*img;
	int			bpp;
	int			sizeline;
	int			endian;
	int			width;
	int			height;
};

struct			s_color
{
	double		r;
	double		g;
	double		b;
};

struct			s_data
{
	t_color			color_final;
	t_ray			*ray;
	t_vector		new_start;
	t_vector		normal;
	int				level;
	double			coef;
	double			t;
};

struct			s_camera
{
	t_vector	origin;
};

struct			s_env
{
	t_image			*image;
	void			*mlx;
	void			*win;
	t_vector		win_size;
	t_scene			*scene;
	t_data			*data_scene;
	t_camera		*camera;
	double			pitch;
	double			yaw;
	double			roll;
	t_intersect		functions[4];
	t_normale		n_fct[4];
	double			win_ratio;
};

struct			s_scene
{
	t_material	**material_tab;
	t_light		**light_tab;
	t_obj		**obj_tab;
	int			obj_count;
	int			light_count;
	int			material_count;
};

struct			s_tmp
{
	int			obj_tmp_count;
	int			light_tmp_count;
	int			material_tmp_count;
};

struct			s_light
{
	t_vector	pos;
	t_color		intensity;
};

struct			s_material
{
	t_color		diffuse;
	double		ref;
};

/*
**color
*/

unsigned int	create_color(int a, int r, int g, int b);
t_color			color_percent(double r, double g, double b);

/*
**image
*/

t_image			*create_image(void *mlx, int width, int height);
void			image_put_pixel(t_env env, int x, int y,
				unsigned int color);
void			clear_image(t_image *image);

/*
**constructor
*/

int				constructor(t_env *env, char *fname);
t_env			*constructor_env(int win_x, int win_y);
int				constructor_add_object(char *line, t_scene *scene,
				t_tmp *obj_val);
t_scene			*constructor_scene(char *file);
int				constructor_add_count(char **line_split, t_scene *scene,
				t_tmp *obj_val);

/*
**constructor_add_object
*/

int				constructor_add_sphere(char **line_split, t_scene *scene,
				t_tmp *obj_val);
int				constructor_add_light(char **line_split, t_scene *scene,
				t_tmp *obj_val);
int				constructor_add_material(char **line_split, t_scene *scene,
				t_tmp *obj_val);
int				constructor_add_plane(char **line_split, t_scene *scene,
				t_tmp *obj_val);
int				constructor_add_cylinder(char **line_split, t_scene *scene,
				t_tmp *obj_val, int type);

/*
**draw_image
*/

void			draw_loop(t_env *env);
int				check_object(t_env *env, t_ray *ray);

/*
**events
*/

int				press_key(int key_code, t_env *env);
int				mouse(int button, int x, int y, t_env *env);
int				mouse_motion(int x, int y, t_env *env);
int				expose(t_env *env);

/*
**objects formula
*/

int				intersect_sphere(t_obj *sphere, t_ray *ray, double *t);
int				intersect_plane(t_obj *plane, t_ray *ray, double *t);
int				intersect_cylinder(t_obj *cylin, t_ray *ray, double *t);
int				intersect_cone(t_obj *cone, t_ray *ray, double *t);
int				resolv_quadratic(double discr, double *t, double b, double a);
int				find_normal_plane(t_env *env, int current);
int				find_normal_sphere(t_env *env, int current);
int				find_normal_cylinder(t_env *env, int current);
int				find_normal_cone(t_env *env, int current);

/*
**vector
*/

t_vector		create_vector(double x, double y, double z);
double			dot_product(t_vector *a, t_vector *b);
t_vector		substract_vector(t_vector *a, t_vector *b);
t_vector		add_vector(t_vector *a, t_vector *b);
t_vector		vector_scale(t_vector *a, double k);

/*
**formula
*/

t_vector		normalize(t_vector *vector);
t_vector		cross_product(t_vector *a, t_vector *b);
double			dist_vector(t_vector *a, t_vector *b);

/*
**light
*/

void			light_function(t_env *env, int current);
void			lamber_diffusion(t_env *env, int i, int current, t_ray *light);

# define CALC	tan(M_PI * 0.5f * 55.0f / 180.0f);
# define ZOOM	0.52056705055
#endif
